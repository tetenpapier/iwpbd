package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Group struct {
	Id      primitive.ObjectID   `bson:"_id" json:"id", omitempty`
	Name    string               `bson:"name" json:"name"`
	Desc    string               `bson:"desc"`
	NbMax   int                  `bson:"nbMax"`
	Members []primitive.ObjectID `bson:"members"`
	Game    primitive.ObjectID   `bson:"game" json:"game"`
}

func NewGroup() *Group {
	return &Group{primitive.NewObjectID(), "", "", 0, []primitive.ObjectID{}, primitive.NewObjectID()}
}

func NewGroupNameDesc(name string, desc string) *Group {
	ret := NewGroup()
	ret.Name = name
	ret.Desc = desc
	return ret
}
