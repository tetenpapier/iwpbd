package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Game struct {
	Id              primitive.ObjectID `bson:"_id", omitempty`
	AppId           uint               `bson:"appid"`
	HasAchievements bool               `bson:"hasAchievements"`
	IsCoop          bool               `bson:"isCoop"`
	IsMulti         bool               `bson:"isMulti"`
	Name            string             `bson:"name" json:"name"`
}

func NewGame() *Game {
	return &Game{primitive.NewObjectID(), 0, false, false, false, ""}
}

func NewGameName(appid uint, name string) *Game {
	ret := NewGame()
	ret.AppId = appid
	ret.Name = name
	return ret
}
