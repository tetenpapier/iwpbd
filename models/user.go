package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	Id          primitive.ObjectID `bson:"_id" json:"id", omitempty`
	Banned      bool               `bson:"banned" json:"banned"`
	BlackLister bool               `bson:"blacklisted" json:"blacklisted"`
	Experience  uint               `bson:"experience" json:"experience"`
	//LastBuy     string `bson:"lastBuy,omitempty"`
	Level       uint   `bson:"level" json:"level"`
	Money       uint   `bson:"money" json:"money"`
	MoneyLimit  uint   `bson:"moneyLimit" json:"moneylimit"`
	DiscordId   string `bson:"userId" json:"discordid"`
	SteamId     string `bson:"steamId" json:"steamid"`
	DiscordName string `bson:"username,omitempty" json:"discordname"`
}

func NewUser() *User {
	return &User{primitive.NewObjectID(), false, false, 0, 0, 0, 0, "", "", ""}
}

func NewUserDiscord(id string, name string) *User {
	ret := NewUser()
	ret.DiscordId = id
	ret.DiscordName = name
	return ret
}

func NewUserSteam(id string) *User {
	ret := NewUser()
	ret.SteamId = id
	return ret
}
