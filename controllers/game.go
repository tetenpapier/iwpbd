package controllers

import (
	"cds/dao"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

// @Summary Renvoie les informations sur un jeu.
// @Produce json
// @Success 200 {object} models.Game
// @Failure 403
// @Failure 404
// @Failure 500
// @Router /games/{id} [get]
func GetGameById(c *fiber.Ctx) error {
	token := c.Cookies("token", "")
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		game, err := dao.GetGameById(id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(game)
		}
	}
}
