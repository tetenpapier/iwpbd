package controllers

import (
	"cds/dao"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

type Groupe struct {
	Name   int `json:"name"`
	Max    int `json:"max"`
	Leader int `json:"leader"`
	//Players []Account `json:"players"`
	//Events  []Event   `json:events`
}

/*
type Event struct {
	Id    int `json:"id"`
	Start int `json:"start"`
	End   int `json:"end"`
}
*/

// @Summary Renvoie les informations sur un groupe
// @Tags groups
// @Produce json
// @Success 200 {json} Groupe
// @Failure 400
// @Failure 404
// @Router /groups/{id} [get]
func GetInfos(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		group, err := dao.GetGroupById(id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(group)
		}
	}
}

// @Summary Renvoie les events d'un groupe
// @Tags groups
// @Produce json
// @Success 200 {json} Groupe
// @Failure 400
// @Failure 404
// @Router /groups/{id}/events [get]
func GetEvents(c *fiber.Ctx) error {
	return nil
}

// @Summary Créer un groupe
// @Tags groups
// @Accept json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/create [put]
func CreateGroup(c *fiber.Ctx) error {
	return nil
}

// @Summary Rejoindre un groupe
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/join [put]
func JoinGroup(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		err := dao.AddMemberInGroup(token, id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.SendStatus(fiber.StatusOK)
		}
	}
}

// @Summary Quitte un groupe
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/leave [put]
func LeaveGroupe(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		err := dao.RemoveMemberFromGroup(token, id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.SendStatus(fiber.StatusOK)
		}
	}
}

// @Summary Change le chef d'un groupe
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/switch [put]
func ChangeLeader(c *fiber.Ctx) error {
	return nil
}

// @Summary Prépare un event
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/events/plan [put]
func CreateEvent(c *fiber.Ctx) error {
	return nil
}

// @Summary Change un event
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/events/change [put]
func ChangeEvent(c *fiber.Ctx) error {
	return nil
}

// @Summary Ferme un groupe et distribue les points CDS
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/end [put]
func EndGroup(c *fiber.Ctx) error {
	return nil
}

// @Summary Supprime un groupe sans distribuer de points
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/cancel [delete]
func DeleteGroup(c *fiber.Ctx) error {
	return nil
}

// @Summary Supprime un event
// @Tags groups
// @Accept json
// @Produce json
// @Success 200
// @Failure 400
// @Failure 404
// @Router /groups/events/cancel [delete]
func DeleteEvent(c *fiber.Ctx) error {
	return nil
}
