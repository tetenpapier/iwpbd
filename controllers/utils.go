package controllers

import "github.com/gofiber/fiber/v2"

func checkCookie(c *fiber.Ctx) string {
	return c.Cookies("token", "")
}
