package controllers

import (
	"cds/dao"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetUsers(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		group, err := dao.GetGroupById(id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(group)
		}
	}
}

// @Summary Renvoie les informations sur le compte se trouvant sur le cookie.
// @Produce json
// @Success 200 {object} models.User
// @Failure 403
// @Failure 404
// @Failure 500
// @Router /users/current [get]
func GetCurrentUser(c *fiber.Ctx) error {
	token := c.Cookies("token", "")
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		user, err := dao.GetById(token)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(user)
		}
	}
}

// @Summary Renvoie les informations sur le compte
// @Produce json
// @Success 200 {object} models.User
// @Failure 403
// @Failure 404
// @Failure 500
// @Router /users/{id} [get]
func GetUser(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		user, err := dao.GetById(id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(user)
		}
	}
}

// @Summary Renvoie la liste des groupes d'un utilisateur
// @Produce json
// @Success 200 {array} models.Group "Liste des ids des groupes"
// @Failure 403
// @Failure 404
// @Failure 500
// @Router /users/{id}/groups [get]
func GetGroupsUser(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		groups, err := dao.GetGroupByMember(id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(groups)
		}
	}
}

// @Summary Renvoie la liste des groupes où ne se trouve pas un utilisateur
// @Produce json
// @Success 200 {array} models.Group "Liste des ids des groupes"
// @Failure 403
// @Failure 404
// @Failure 500
// @Router /users/{id}/not/groups [get]
func GetNotGroupsUser(c *fiber.Ctx) error {
	token := checkCookie(c)
	if token == "" {
		return c.SendStatus(fiber.StatusForbidden)
	} else {
		id := c.Params("id")
		groups, err := dao.GetGroupNotByMember(id)
		if err == mongo.ErrNoDocuments {
			return c.SendStatus(fiber.StatusNotFound)
		} else if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": fmt.Sprint(err),
			})
		} else {
			return c.Status(fiber.StatusOK).JSON(groups)
		}
	}
}
