package routes

import (
	"cds/controllers"

	"github.com/gofiber/fiber/v2"
)

func GroupRoute(route fiber.Router) {
	route.Get("/:id", controllers.GetInfos)
	route.Get("/:id/events", controllers.GetEvents)
	route.Put("/create", controllers.CreateGroup)
	route.Put("/:id/join", controllers.JoinGroup)
	route.Put("/:id/leave", controllers.LeaveGroupe)
	route.Put("/switch", controllers.ChangeLeader)
	route.Put("/events/plan", controllers.CreateEvent)
	route.Put("/events/change", controllers.ChangeEvent)
	route.Put("/end", controllers.EndGroup)
	route.Delete("/cancel", controllers.DeleteGroup)
	route.Delete("/events/cancel", controllers.DeleteEvent)
}
