package routes

import (
	"cds/controllers"

	"github.com/gofiber/fiber/v2"
)

func GameRoute(route fiber.Router) {
	route.Get("/:id", controllers.GetGameById)
}
