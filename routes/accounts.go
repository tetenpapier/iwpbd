package routes

import (
	"cds/controllers"

	"github.com/gofiber/fiber/v2"
)

func UserRoute(route fiber.Router) {
	route.Get("/current", controllers.GetCurrentUser)
	route.Get("/:id", controllers.GetUser)
	route.Get("/:id/groups", controllers.GetGroupsUser)
	route.Get("/:id/not/groups", controllers.GetNotGroupsUser)
}
