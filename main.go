package main

import (
	"cds/controllers"
	"cds/dao"
	"cds/models"
	"cds/routes"
	"fmt"
	"os"

	_ "cds/docs"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
	"github.com/joho/godotenv"
	"github.com/markbates/goth"
	"github.com/markbates/goth/providers/discord"
	"github.com/markbates/goth/providers/steam"
	"github.com/shareed2k/goth_fiber"
	"go.mongodb.org/mongo-driver/mongo"
)

//const DISCORD_API = "https://discord.com/api/v10/"

func setupRoutes(app *fiber.App) {
	//app.Get("/", index)
	api := app.Group("/api")
	routes.GroupRoute(api.Group("/groups"))
	routes.UserRoute(api.Group("/users"))
	routes.GameRoute(api.Group("/games"))
	app.Get("/swagger/*", swagger.HandlerDefault)

	app.Get("/login/:provider", goth_fiber.BeginAuthHandler)
	app.Get("/auth/:provider", controllers.Auth)
	app.Get("/check", controllers.CheckAuth)
	//app.Get("/groups/:id", groups)
}

}

}

func index(c *fiber.Ctx) error {
	token := c.Cookies("token", "")
	var discord string = ""
	var steam string = ""
	var listGroups []models.Group
	if token != "" {
		user, _ := dao.GetById(fmt.Sprint(token))
		if user != nil {
			steam = user.SteamId
			discord = user.DiscordName
			listGroups, _ = dao.GetGroupByMember(fmt.Sprint(token))
		}
	}

	return c.Render("index", fiber.Map{
		"connected":   token != "",
		"urlDiscord":  "/login/discord",
		"urlSteam":    "/login/steam",
		"discordName": discord,
		"steamName":   steam,
		"listGroups":  listGroups,
	})
}

func groups(c *fiber.Ctx) error {
	groupId := c.Params("id")
	group, err := dao.GetGroupById(groupId)
	if err == mongo.ErrNoDocuments {
		return c.SendStatus(fiber.StatusNotFound)
	} else if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	token := c.Cookies("token", "")
	joined, _ := dao.CheckMemberInGroup(fmt.Sprint(token), groupId)
	game, _ := dao.GetGameById(group.Game.Hex())

	return c.Render("group", fiber.Map{
		"name":   group.Name,
		"game":   game.Name,
		"joined": joined,
	})
}

// @title           CDS API
// @version         1.0 (alpha)
// @description     API du site web de CDS
// @contact.name   La super équipe de dev
// @license.name  AGPL 3.0
// @license.url   https://www.gnu.org/licenses/agpl-3.0.html
// @host      localhost:8080
// @BasePath  /api
func main() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}

	app := fiber.New()
	goth.UseProviders(
		discord.New(
			os.Getenv("DISCORD_ID"),
			os.Getenv("DISCORD_SECRET"),
			"http://localhost:8080/auth/discord",
			discord.ScopeIdentify, discord.ScopeGuilds),
		steam.New(os.Getenv("STEAM_KEY"), "http://localhost:8080/auth/steam"),
	)

	setupRoutes(app)
	app.Static("/", "./dist")
	app.Static("/*", "./dist")

	err = app.Listen(":8080")
	if err != nil {
		panic(err)
	}
}
