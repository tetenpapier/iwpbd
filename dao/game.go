package dao

import (
	"cds/models"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetGameByAppId(id uint) (game *models.Game, err error) {
	db, _ := get()
	defer disconnect(db.Client())
	coll := db.Collection("games")

	var result models.Game
	err = coll.FindOne(context.TODO(), bson.D{{"appid", id}}).Decode(&result)
	if err != nil {
		return nil, err
	} else {
		return &result, nil
	}
}

func GetGameById(id string) (game *models.Game, err error) {
	db, _ := get()
	defer disconnect(db.Client())
	coll := db.Collection("games")
	objectId, err := primitive.ObjectIDFromHex(id)

	var result models.Game
	err = coll.FindOne(context.TODO(), bson.D{{"_id", objectId}}).Decode(&result)
	if err != nil {
		return nil, err
	} else {
		return &result, nil
	}
}
