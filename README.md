# Site CDS

## Structure du projet

 * .env : fichier de configuration
 * controllers : actions possibles par l'API REST
 * docs : doc swagger (swagger -i)
 * routes : configuration des routes
 * views : fichiers html dynamiques
 * dao : fichiers pour faire des requetes en BDD
 * models : structures utilisées pour manipuler le contenu de la base de
   données

## Lancement

### Frontend

Le frontend utilise **Rust** et **WASM**. Il vous faut Trunk en plus de Cargo
pour pouvoir compiler les fichiers Rust en WASM. Il faut en premier installer
les dépendances JS puis compiler les fichiers avec cargo puis Trunk.

```
cd front
npm install
cargo build
trunk build
```

JS est nécessaire pour une crate qui permet d'utiliser les propriétés d'une
bibliothèque JS.

### Backend

Il faut mettre en place une base de données
[MongoDB](https://www.mongodb.com/).  Il est nécessaire d'avoir [une
application Discord](https://discord.com/developers/) et [une clé d'API
Steam](https://steamcommunity.com/dev).  Configurez ensuite le fichier
`example.env` et renommez le en `.env`.

Lancez le serveur avec `go run main.go`.

### Documentation

Installer swager avec Go et lancer le. Vous pouvez démarrer le serveur et aller
sur le lien [de la documentation](localhost:8080/swagger) pour pouvoir la lire
en ligne.

```
go install github.com/swaggo/swag
swag init
```
