mod home;
mod list_groups;

pub use home::*;
use list_groups::ListGroups;

use gloo_net::http::Request;
use gloo_net::Error;
use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Eq, PartialEq, Serialize)]
pub struct User {
    id: String,
    banned: bool,
    blacklisted: bool,
    experience: u32,
    level: u32,
    money: u32,
    moneylimit: u32,
    steamid: String,
    discordid: String,
    discordname: String,
}

#[derive(Deserialize, Serialize)]
struct Game {
    name: String,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct Group {
    pub id: String,
    pub name: String,
    pub game: String,
}

impl Group {
    pub async fn get_game_name(&mut self) {
        match Request::get(&format!("/api/games/{}", self.game))
            .send()
            .await
        {
            Ok(resp) => {
                if resp.ok() {
                    let json: Result<Game, Error> = resp.json().await;
                    match json {
                        Ok(json) => self.game = json.name,
                        Err(err) => log::info!("{}", err.to_string()),
                    }
                }
            }
            Err(_) => (),
        }
    }
}
