use gloo_net::http::Request;
use gloo_net::Error;
use gloo_storage::errors::StorageError;
use gloo_storage::{LocalStorage, Storage};
use patternfly_yew::{Toast, ToastDispatcher, ToastViewer, Type};
use yew::prelude::*;

use crate::components::{ListGroups, User};

pub struct Home {
    user: Option<User>,
}

#[derive(Eq, PartialEq)]
pub enum TestMessage {
    Toast(ToastType),
    ConnexionOk(User),
}

#[derive(Eq, PartialEq)]
pub struct ToastFields {
    pub title: String,
    pub body: String,
}

#[derive(Eq, PartialEq)]
pub enum ToastType {
    Info(ToastFields),
    Error(ToastFields),
}

impl Component for Home {
    type Message = TestMessage;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self { user: None }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let user: Result<User, StorageError> = LocalStorage::get("auth");
        if let Ok(user) = user {
            if self.user.is_none() {
                ctx.link().send_message(TestMessage::ConnexionOk(user));
            }
        } else {
            ctx.link().send_future_batch(async {
                match Request::get("/api/users/current").send().await {
                    Ok(resp) => {
                        if resp.ok() {
                            let json: Result<User, Error> = resp.json().await;
                            match json {
                                Ok(json) => {
                                    let user = json.clone();
                                    match LocalStorage::set("auth", json) {
                                        Ok(_) => vec![
                                            TestMessage::Toast(ToastType::Info(ToastFields {
                                                title: String::from("Connexion réussie !"),
                                                body: String::new(),
                                            })),
                                            TestMessage::ConnexionOk(user),
                                        ],
                                        Err(err) => vec![TestMessage::Toast(ToastType::Error(
                                            ToastFields {
                                                title: String::from("Erreur lors de l'ajout du token dans le LocalStorage."),
                                                body: err.to_string(),
                                            },
                                        ))],
                                    }
                                }
                                Err(err) => {
                                    LocalStorage::delete("auth");
                                    vec![TestMessage::Toast(ToastType::Error(ToastFields {
                                        title: String::from("Connexion: JSON non valide."),
                                        body: err.to_string(),
                                    }))]
                                }
                            }
                        } else {
                            vec![TestMessage::Toast(ToastType::Error(ToastFields {
                                title: String::from("Error from the promise"),
                                body: String::from("L'API ne retourne pas un code 200."),
                            }))]
                        }
                    }
                    Err(err) => vec![TestMessage::Toast(ToastType::Error(ToastFields {
                        title: String::from("Connexion: Erreur lors de la tentative de la connexion."),
                        body: err.to_string(),
                    }))],
                }
            });
        }

        html! {
            <>
            <ToastViewer />
            if let Some(user) = &self.user {
                <h1>{ format!("Bienvenue {} !", user.discordname) }</h1>
                    if user.discordid.is_empty() {
                        <p><a href="/login/discord">{ "SE CONNECTER VIA DISCORD ICI" }</a></p>
                    }

                    if user.steamid.is_empty() {
                        <p><a href="/login/steam">{ "SE CONNECTER VIA STEAM ICI" }</a></p>
                    }
                <div>
                <ListGroups id={user.id.clone()} />
                </div>
            } else {
                <p><a href="/login/discord">{ "SE CONNECTER VIA DISCORD ICI" }</a></p>
                <p><a href="/login/steam">{ "SE CONNECTER VIA STEAM ICI" }</a></p>
            }
            </>
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            TestMessage::ConnexionOk(user) => {
                self.user = Some(user);
                return true;
            }
            TestMessage::Toast(toast) => match toast {
                ToastType::Info(infos) => ToastDispatcher::new().toast(Toast {
                    title: infos.title,
                    r#type: Type::Info,
                    ..Default::default()
                }),
                ToastType::Error(infos) => ToastDispatcher::new().toast(Toast {
                    title: infos.title,
                    r#type: Type::Danger,
                    body: html! { <p>{ infos.body }</p> },
                    ..Default::default()
                }),
            },
            _ => ToastDispatcher::new().toast(Toast {
                title: String::from("Un callback n'a pas été implémenté !"),
                r#type: Type::Warning,
                ..Default::default()
            }),
        }

        false
    }
}
