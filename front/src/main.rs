mod components;

use components::Home;

use gloo_net::http::Request;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/about")]
    About,
    #[at("/test")]
    Test,
    #[at("/404")]
    NotFound,
}

#[function_component(About)]
fn about() -> Html {
    html! {
        <>
        <h1>{ "À propos" }</h1>
        </>
    }
}

fn switch(route: &Route) -> Html {
    match route {
        Route::Home => html! { <Home /> },
        Route::About => html! { <About /> },
        Route::NotFound => html! { <h1>{"404, déso"}</h1> },
        Route::Test => html! { <Test /> },
    }
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={Switch::render(switch)} />
        </BrowserRouter>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::start_app::<App>();
}

#[function_component(Test)]
fn test() -> Html {
    let test = use_state(|| String::from(""));
    {
        let test = test.clone();
        wasm_bindgen_futures::spawn_local(async move {
            if let Ok(o) = Request::get("/test").send().await {
                test.set(o.text().await.unwrap())
            } else {
                test.set(String::from("Pas de connexion !!"))
            };
        });
    }

    html! {
        <>
        <h1>{ "Moi !!!" }</h1>
        <p>{ (*test).clone() }</p>
        </>
    }
}
